using System;
using NUnit.Framework;
using proj.Classes;
using System.Linq;
using proj.Data;
using System.Collections.Generic;
using proj.Interfaces;

namespace test
{
    public class Tests
    {

        TurtleGame myTurtleGame;

        [SetUp]
        public void SetUp()
        {
            myTurtleGame = new TurtleGame();
        }

        [TestCase(typeof(Turtle))]
        [TestCase(typeof(Exit))]
        [TestCase(typeof(Mine))]
        public void CheckThatGridHasItem(Type type)
        {
            //Arrange
            //Act
            bool wasFound = false;

            foreach(var item in myTurtleGame.Grid)
            {
                if(item != null && item.GetType() == type)
                {
                    wasFound = true;
                }
            }

            //Assert
            Assert.That(wasFound == true);
        }

        [TestCase(typeof(Turtle), 0, 1)]
        [TestCase(typeof(Exit), 4, 2)]
        public void CHeckThatExitAndTurtleAreOnTheRightPosiion(Type type, int x, int y)
        {
            //Arrange
            //Act
            bool wasFound = false;
            var row = 0;
            var col = 0;

            foreach(var item in myTurtleGame.Grid)
            {
                if(item != null && item.GetType() == type)
                {
                    wasFound = true;
                    row = item.X;
                    col = item.Y;
                }
            }

            //Assert
            Assert.That(wasFound == true);
            Assert.That(row == x);
            Assert.That(col == y);
        }

        [Test]
        public void CheckThatTurtleCanRotate()
        {
            //Arrange
            //Act
            var result = myTurtleGame.MoveTurtleOnTheGrid("r");

            //Assert
            Assert.That(result.SuccessFailure == SuccessFailure.Success);
        }
        

        [Test]
        public void CheckThatTurtleCanMove_With_Success()
        {
            //Arrange
            //removing mines from the grid
            myTurtleGame.Mines.ToList().Clear();

            //Act
            var result = myTurtleGame.MoveTurtleOnTheGrid("m");

            //Assert
            Assert.That(result.SuccessFailure == SuccessFailure.Success);
        }

        [Test]
        public void CheckThatTurtle_Suffers_Consequences_On_Hitting_Mine()
        {
            //Arrange
            //removing mines from the grid
            myTurtleGame.Grid[0, 0] = new Mine();

            //Act
            var result = myTurtleGame.MoveTurtleOnTheGrid("m");

            //Assert
            Assert.That(result.SuccessFailure == SuccessFailure.Falure);
            Assert.That(result.Message.Equals("The turtle hit a mine"));
        }

        [Test]
        public void CheckThatTurtle_Can_Successfully_Reach_The_Exit()
        {
            //Arrange
            //removing mines from the grid
            myTurtleGame.Grid[0, 0] = new Exit();

            //Act
            var result = myTurtleGame.MoveTurtleOnTheGrid("m");

            //Assert
            Assert.That(result.SuccessFailure == SuccessFailure.FinalSuccess);
            Assert.That(result.Message.Equals("Sucsses. You reached the exit:)"));
        }

        [Test]
        public void CheckThatTurtle_Can_Successfully_Reach_The_Exit2()
        {
            //Arrange
            //removing mines from the grid
            foreach(var item in myTurtleGame.Grid)
            {
                if(item != null && item.GetType() == typeof(Mine))
                {
                    myTurtleGame.Grid[item.X, item.Y] = null;
                }
            }

            var ls = myTurtleGame.Mines.ToList();
            TestContext.WriteLine("*** Count: " + ls.Count);
            foreach(var item in ls)
            {
                item.X = 3;
                item.Y = 0;
            }

            //Act
            myTurtleGame.MoveTurtleOnTheGrid("r");

            myTurtleGame.MoveTurtleOnTheGrid("m");
            myTurtleGame.MoveTurtleOnTheGrid("m");
            myTurtleGame.MoveTurtleOnTheGrid("m");
            myTurtleGame.MoveTurtleOnTheGrid("m");

            myTurtleGame.MoveTurtleOnTheGrid("r");
            var result = myTurtleGame.MoveTurtleOnTheGrid("m");

            //Assert
            Assert.That(result.SuccessFailure == SuccessFailure.FinalSuccess);
        }

        [Test]
        public void checkThatGameFailsIfFileNotFound()
        {
            //Arrange - act - assert
            Assert.Throws<Exception>(() => myTurtleGame.MoveTurtleOnTheGridWithMultipleMoves("WRONG_file"));
        }
    }
}