namespace proj.Data
{
    public enum RotationPosition
    {
        Up,
        Right,
        Down,
        Left
    }
}