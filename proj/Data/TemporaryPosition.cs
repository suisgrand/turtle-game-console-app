using proj.Interfaces;

namespace proj.Data
{
    public class TemporaryPosition : IPiece
    {
        public int X { get; set; }
        public int Y { get; set; }
    }
}