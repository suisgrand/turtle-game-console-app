namespace proj.Data
{
    public class TurtleMoveConsequence
    {
        public SuccessFailure SuccessFailure { get; set; }
        public string Message { get; set; }
    }
}