﻿using System;
using System.IO;
using proj.Classes;

namespace proj
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello User. Only r (rotate) or m (move) are allowed)");
            System.Console.WriteLine();
            var turtleGame = new TurtleGame();

            if(args[0] != null && !string.IsNullOrEmpty(args[0]))
            {
                turtleGame.MoveTurtleOnTheGridWithMultipleMoves(args[0]);
            }
            else{
                bool bContinue = true;

                while(bContinue)
                {

                    System.Console.WriteLine("type in next move");
                    var line = Console.ReadLine();
                    if(line.Equals("x", StringComparison.InvariantCultureIgnoreCase))
                    {
                        bContinue = false;
                    }

                    var cmd = turtleGame.MoveTurtleOnTheGrid(line);
                    if(cmd.Message != null)
                    {
                        bContinue = false;
                        System.Console.WriteLine("End of the game: " + cmd.Message);
                    }
                }
            }


        }
    }
}
