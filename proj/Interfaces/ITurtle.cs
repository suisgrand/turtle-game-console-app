using proj.Data;

namespace proj.Interfaces
{
    public interface ITurtle: IPiece
    {
        RotationPosition Rotation { get; set; }
        SuccessFailure Rotate();
    }
}