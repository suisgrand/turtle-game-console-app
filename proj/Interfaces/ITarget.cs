using proj.Data;

namespace proj.Interfaces
{
    public interface ITarget: IPiece
    {
        TurtleMoveConsequence Hit();

    }
}