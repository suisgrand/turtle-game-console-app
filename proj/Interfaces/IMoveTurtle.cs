using proj.Data;

namespace proj.Interfaces
{
    public interface IMoveTurtle
    {
        TemporaryPosition Move(ITurtle turtle);
    }
}