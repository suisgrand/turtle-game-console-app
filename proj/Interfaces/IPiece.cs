namespace proj.Interfaces
{
    public interface IPiece
    {
        int X { get; set; }
        int Y { get; set;  }
    }
}