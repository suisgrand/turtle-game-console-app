using proj.Data;

namespace proj.Interfaces
{
    public interface IRotate
    {
        SuccessFailure Rotate(ITurtle turtle);
    }
}