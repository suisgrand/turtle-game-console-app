using proj.Classes;
using proj.Interfaces;

namespace proj.Utilities
{
    public static class FactoryPattern
    {
        public static ITurtle CreateTurtle()
        {
            return new Turtle();
        }

        public static ITarget CreateMine()
        {
            return new Mine();
        }

        public static ITarget CreateExit()
        {
            return new Exit();
        }

        public static IMoveTurtle CreateMoveTurtle()
        {
            return new MoveTurtle();
        }

    }
}