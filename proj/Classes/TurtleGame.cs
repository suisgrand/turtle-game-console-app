using proj.Interfaces;
using proj.Utilities;
using proj.Constants;
using proj.Data;
using System.Collections.Generic;
using System.Linq;
using System;
using proj.Extensions;
using System.IO;

namespace proj.Classes
{
    public class TurtleGame
    {
        private readonly ITurtle Turtle;
        private readonly ITarget Exit;
        public ITarget[] Mines { get; set; }
        public IPiece[,] Grid { get; private set;}
        private readonly IMoveTurtle MoveTurtle;

        private delegate PositionsWithGridDimension GetPositions();
        private delegate TurtleMoveConsequence CheckNewPosition(IPiece tempoaryPosition);
        private readonly List<CheckNewPosition> ListCheckPositions;

        public TurtleGame()
        {

            this.Turtle = FactoryPattern.CreateTurtle();
            this.MoveTurtle = FactoryPattern.CreateMoveTurtle();

            this.Exit = FactoryPattern.CreateExit();
            this.Grid = new IPiece[ Constant.RowNumber, Constant.ColumnNumber ];

            this.Mines = new ITarget[Constant.NumberOfMines];
            for (int i = 0; i < this.Mines.Length; i++)
            {
                this.Mines[i] = FactoryPattern.CreateMine();
            }

            ListCheckPositions = new List<CheckNewPosition>();
            ListCheckPositions.Add(IsPositionOffBounds);
            ListCheckPositions.Add(HasTurtleReachedPiece<Mine>);
            ListCheckPositions.Add(HasTurtleReachedPiece<Exit>);

            InitDefaultPositions();
        }
        
        private void InitDefaultPositions()
        {
            var myPieces = new List<IPiece>();

            this.Turtle.X = Constant.DefaultTurtleRow;
            this.Turtle.Y = Constant.DefaultTurtleColumn;
            this.Turtle.Rotation = Constant.DefaultTurtleRotation;

            if(! CheckIfCoordinatesAreCorrect(this.Turtle)){
                throw new Exception(Constant.GridWrongSettings);
            }

            myPieces.Add(this.Turtle);
            
            this.Exit.X = Constant.DefaultExitRowPosition;
            this.Exit.Y = Constant.DefaultExitColumnPosition;

            myPieces.Add(this.Exit);

            if(! CheckIfCoordinatesAreCorrect(this.Exit)){
                throw new Exception(Constant.GridWrongSettings);
            }

            for (int i = 0; i < this.Mines.Length; i++)
            {
                this.Mines[i].X = GetRandomValue(GetTurtleAndExitRows);
                this.Mines[i].Y = GetRandomValue(GetTurtleAndExitColumns);
                myPieces.Add(this.Mines[i]);
            }

            foreach(var item in myPieces)
            {
                this.Grid[item.X, item.Y] = item;
            }
        }

        private bool CheckIfCoordinatesAreCorrect(IPiece item)
        {
            if(item.X < 0 || item.X > Constant.RowNumber)
            {
                return false;
            }

            if(item.Y < 0 || item.Y > Constant.ColumnNumber)
            {
                return false;
            }
            return true;
        }


        public TurtleMoveConsequence MoveTurtleOnTheGridWithMultipleMoves(string fileName)
        {
            var fileContents = string.Empty;
            var result = new TurtleMoveConsequence { SuccessFailure = SuccessFailure.ErrorFromProcessingFile };
            try
            {
                using (var sr = new StreamReader(fileName))
                {
                    fileContents = sr.ReadToEnd();
                }

                var moves = fileContents.Split(',', StringSplitOptions.RemoveEmptyEntries);
                for (int i = 0; i < moves.Length; i++)
                {
                    result = this.MoveTurtleOnTheGrid(moves[i]);
                    {
                        if(result.SuccessFailure == SuccessFailure.Falure)
                        {
                            return result;
                        }
                    }
                }
            }
            catch (System.Exception e)
            {
                throw new Exception("the project could not be run: error: " + e.Message);
            }
            
            return result;
        }

        public TurtleMoveConsequence MoveTurtleOnTheGrid(string move)
        {
            
            var userMove = move.TranslateMove();
            
            if(userMove == TurtleMove.NotRecognized)
            {
                return new TurtleMoveConsequence {  Message = Constant.WrongUserInput};
            }
            System.Console.WriteLine($" BEFORE: current rotation: {this.Turtle.Rotation} x: {this.Turtle.X}, Y: {this.Turtle.Y} userMove: {userMove}");
            var result = Move(userMove);
            if(result.SuccessFailure == SuccessFailure.Success)
            {
                    System.Console.WriteLine($" AFTER: current rotation: {this.Turtle.Rotation} x: {this.Turtle.X}, Y: {this.Turtle.Y} userMove: {userMove}");
            }
            
            return result;
        }

        private TurtleMoveConsequence Move(TurtleMove turtleMove)
        {
            var result = new TurtleMoveConsequence
            {
                SuccessFailure = SuccessFailure.Falure
            };

            if(turtleMove == TurtleMove.Rotate)
            {
                result.SuccessFailure = this.Turtle.Rotate();
                if(result.SuccessFailure == SuccessFailure.Falure){
                    result.Message = Constant.RotationNotWorking;
                }
                return result;
            }
            
            if(turtleMove == TurtleMove.Move)
            {
                var newPosition = this.MoveTurtle.Move(this.Turtle);
                return MoveCheck(newPosition);
            }

            return result;
        }

        private TurtleMoveConsequence MoveCheck(IPiece temporaryPosition)
        {
            foreach(var item in this.ListCheckPositions)
            {
                var result = item.Invoke(temporaryPosition);
                if(result.SuccessFailure == SuccessFailure.Falure)
                {
                    //display mines
                    System.Console.WriteLine("The mines were on the following cells");
                    foreach(var mine in this.Mines)
                    {
                        System.Console.WriteLine($"mine: x: {mine.X} -- y: {mine.Y}");
                    }
                    return result;
                }

                if(result.SuccessFailure == SuccessFailure.FinalSuccess)
                {
                    System.Console.WriteLine(result.Message);
                    return result;
                }
            }

            this.Turtle.X = temporaryPosition.X;
            this.Turtle.Y = temporaryPosition.Y;
            return new TurtleMoveConsequence { SuccessFailure = SuccessFailure.Success };
        }

        private TurtleMoveConsequence IsPositionOffBounds(IPiece tempoaryPosition)
        {

            var result = new TurtleMoveConsequence
            {
                SuccessFailure = SuccessFailure.Falure,
                Message = Constant.OutOfBounds
            };

            if( (tempoaryPosition.X >= 0 && tempoaryPosition.X <= Constant.RowNumber) 
                && (tempoaryPosition.Y >= 0 && tempoaryPosition.Y <= Constant.ColumnNumber) )
            {
                
                result.SuccessFailure = SuccessFailure.Success;
                result.Message = "";
            }
            return result;
        }

        private TurtleMoveConsequence HasTurtleReachedPiece<T>(IPiece tempoaryPosition)
        {
            var result = new TurtleMoveConsequence
            {
                SuccessFailure = SuccessFailure.Falure,
                Message =  Constant.MineWasTriggered
            };

            if(typeof(T) == typeof(Exit))
            {
                result = new TurtleMoveConsequence
                {
                    SuccessFailure = SuccessFailure.FinalSuccess,
                    Message =  Constant.SuccessReachedExit
                };
            }

            IPiece cell = this.Grid[tempoaryPosition.X, tempoaryPosition.Y];
            if( !(this.Grid[tempoaryPosition.X, tempoaryPosition.Y] is T))
            {
                result.SuccessFailure = SuccessFailure.Success;
                result.Message = "";
            }
            return result;
        }


        
        private PositionsWithGridDimension GetTurtleAndExitRows()
        {
            var result = new List<int> { this.Turtle.X, this.Exit.X };
            return new PositionsWithGridDimension
            {
                Dimension = Constant.RowNumber,
                Positions = result.ToArray()
            };
        }

        private PositionsWithGridDimension GetTurtleAndExitColumns()
        {
            var result = new List<int> { this.Turtle.X, this.Exit.Y };
            return new PositionsWithGridDimension
            {
                Dimension = Constant.ColumnNumber,
                Positions = result.ToArray()
            };
        }
        
        private int GetRandomValue(GetPositions positions)
        {
            var item = positions.Invoke();
            var range = Enumerable.Range(0, item.Dimension).Where(x => ! item.Positions.Contains(x));
            var rand = new Random();
            int index = rand.Next(0, item.Dimension - item.Positions.Count());
            return range.ElementAt(index);
        }

        private class PositionsWithGridDimension
        {
            public int Dimension { get; set; }
            public int[] Positions { get; set; }
        }
    }
}