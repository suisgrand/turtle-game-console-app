using proj.Data;
using proj.Interfaces;

namespace proj.Classes
{
    public class RotateRight: IRotate
    {
        public SuccessFailure Rotate(ITurtle turtle)
        {
            if(turtle.Rotation == RotationPosition.Right)
            {
                turtle.Rotation = RotationPosition.Down;
                return SuccessFailure.Success;
            }

            return SuccessFailure.Falure;
        }
    }
}