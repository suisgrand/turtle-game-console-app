using proj.Data;
using proj.Interfaces;

namespace proj.Classes
{
    public class RotateLeft: IRotate
    {
        public SuccessFailure Rotate(ITurtle turtle)
        {
            if(turtle.Rotation == RotationPosition.Left)
            {
                turtle.Rotation = RotationPosition.Up;
                return SuccessFailure.Success;
            }

            return SuccessFailure.Falure;
        }
    }
}