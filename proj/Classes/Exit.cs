using proj.Data;
using proj.Interfaces;

namespace proj.Classes
{
    public class Exit: ITarget
    {
        public int X { get; set; }
        public int Y { get; set; }
        public TurtleMoveConsequence Hit()
        {
            throw new System.NotImplementedException();
        }
    }
}