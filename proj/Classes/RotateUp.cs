using proj.Data;
using proj.Interfaces;

namespace proj.Classes
{
    public class RotateUp : IRotate
    {
        public SuccessFailure Rotate(ITurtle turtle)
        {
            if(turtle.Rotation == RotationPosition.Up)
            {
                turtle.Rotation = RotationPosition.Right;
                return SuccessFailure.Success;
            }

            return SuccessFailure.Falure;
        }
    }
}