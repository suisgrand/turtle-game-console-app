using proj.Data;
using proj.Interfaces;

namespace proj.Classes
{
    public class Turtle : ITurtle
    {
        public int X { get; set; }
        public int Y { get; set; }
        public RotationPosition Rotation { get; set; }

         public IRotate[] Rotations { get; set; }

        public Turtle()
        {
            this.Rotations = new IRotate[4];
            this.Rotations[0] = new RotateLeft();
            this.Rotations[1] = new RotateRight();
            this.Rotations[2] = new RotateUp();
            this.Rotations[3] = new RotateDown();
        }

        public SuccessFailure Rotate()
        {
            for (int i = 0; i < this.Rotations.Length; i++)
            {
                if(this.Rotations[i].Rotate(this) == SuccessFailure.Success)
                {
                    return SuccessFailure.Success;
                }
            }
            return SuccessFailure.Success;
        }
    }
}