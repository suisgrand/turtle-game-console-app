using proj.Interfaces;

namespace proj.Classes
{
    public class Piece : IPiece
    {
        public int X { get; set; }
        public int Y { get; set; }
    }
}