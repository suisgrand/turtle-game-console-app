using proj.Data;
using proj.Interfaces;

namespace proj.Classes
{
    public class RotateDown: IRotate
    {
        public SuccessFailure Rotate(ITurtle turtle)
        {
            if(turtle.Rotation == RotationPosition.Down)
            {
                turtle.Rotation = RotationPosition.Left;
                return SuccessFailure.Success;
            }

            return SuccessFailure.Falure;
        }
    }
}