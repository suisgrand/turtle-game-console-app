
using proj.Constants;
using proj.Data;
using proj.Interfaces;

namespace proj.Classes
{
    public class MoveTurtle: IMoveTurtle
    {
        private ITurtle Turtle { get; set; }

        public TemporaryPosition Move(ITurtle turtle)
        {
            var result = new TemporaryPosition
            {
                X = turtle.X,
                Y = turtle.Y
            };
            switch(turtle.Rotation)
            {
                case RotationPosition.Up:
                    result.Y -= Constant.TurtleMoveStep;
                    break;
                case RotationPosition.Right:
                    result.X += Constant.TurtleMoveStep;
                    break;
                case RotationPosition.Down:
                    result.Y += Constant.TurtleMoveStep;
                    break;
                case RotationPosition.Left:
                    result.X -= Constant.TurtleMoveStep;
                    break;
                default:
                    break;
            }
            return result;
        }


        
    }
}