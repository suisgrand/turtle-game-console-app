namespace proj.Classes
{
    public enum TurtleMove
    {
       Move,
       Rotate,
       NotRecognized
    }
}