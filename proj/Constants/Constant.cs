using proj.Data;

namespace proj.Constants
{
    public static class Constant
    {
        public static readonly int RowNumber = 5;
        public static readonly int ColumnNumber = 4;

        public static readonly int DefaultTurtleRow = 0;
        public static readonly int DefaultTurtleColumn = 1;
        public static readonly RotationPosition DefaultTurtleRotation = RotationPosition.Up;
        public static readonly int TurtleMoveStep = 1;

        public static readonly int DefaultExitRowPosition = 4;
        public static readonly int DefaultExitColumnPosition = 2;

        public static readonly int NumberOfMines = 3;

        public static readonly string RotationNotWorking = "The rotation that you have requested could not be activated";
        public static readonly string OutOfBounds = "The turtle is not on the grid anymore";
        public static readonly string MineWasTriggered = "The turtle hit a mine";
        public static readonly string SuccessReachedExit = "Sucsses. You reached the exit:)";
        public static readonly string WrongUserInput = "User move is not recognized. you can only input m (move) or r (rotate)";


        public static readonly string GridWrongSettings = "One the pieces (Exit or Turtle) are not located on the grid.";
    }
}