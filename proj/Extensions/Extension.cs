using System;
using proj.Classes;

namespace proj.Extensions
{
    public static class Extension
    {
        public static TurtleMove TranslateMove(this string move)
        {
            if(move.Equals("R", StringComparison.CurrentCultureIgnoreCase))
            {
                return TurtleMove.Rotate;
            }

            if(move.Equals("M",  StringComparison.CurrentCultureIgnoreCase)){
                return TurtleMove.Move;
            }

            return TurtleMove.NotRecognized;
        }
    }
}